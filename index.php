<?php
Header('Access-Control-Allow-Origin: *'); //For First Time Allow All Request
Header('Access-Control-Allow-Headers: headers, Origin, X-Requested-With, Content-Type, Accept, Authorization,mobileAccess,MobileAccess'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST'); //Allowed Only Get,POST Method
header('Access-Control-Max-Age: 10000'); // Cache store only 5 minute

date_default_timezone_set("Asia/Kolkata");

error_reporting(1);

require_once 'classes/CommonFunction.php';
require_once 'classes/Config.php';
require_once 'classes/Auth.php';
require_once 'classes/Vehicle.php';

$className = $_POST['className'];
$methodName = $_POST['methodName'];

$obj = new $className();
$obj->$methodName();

header('Content-Type: application/json; charset=utf-8');
//sleep(2);
echo $obj->apiResponseData();