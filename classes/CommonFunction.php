<?php

trait CommonFunction
{

    protected $actionType = array(
        'add' => 'add',
        'edit' => 'edit',
        'delete' => 'delete'
    );

    protected $allStatusTye = array();

    /*
     * Check both value is equal
     * $value1 - Value of compare
     * $value2 - Value with compare
     * return true if value is same otherwise false
     * */
    public function equals($value1, $value2)
    {
        if ($value1 == $value2) {
            return true;
        }
        return false;
    }

    /*
     * Convert null to empty string
     * $string - Value of convert to string if null
     * return value with string
     * */
    public function convertNullToEmptyString($string)
    {
        if ($string == null || $string == '') {
            return '';
        }
        return (string)$string;
    }

    /*
     * Check value is null or empty or zero
     * $value - Value you need check
     * return true if value is have anything otherwise false
     * */

    public function isNotNullOrEmptyOrZero($value)
    {
        if ($value != '' && $value != null && $value != 0) {
            return true;
        }
        return false;
    }

    public function handleSpecialCharacters($string)
    {
        return addslashes(strip_tags(trim($string)));
    }

    public function checkDataIsExistInTable($tableName = '', $columns = array('*'), $conditions = array(), $joins = array(), $otherConditions = '')
    {

        try {

            $values = array();
            $query = "SELECT " . implode(',', $columns) . " FROM " . $tableName;
            if (count($joins) > 0) {
                foreach ($joins AS $join) {
                    $query .= " LEFT JOIN " . $join['tableName'] . " ON " . $join['firstTableColumn'] . " = " . $join['secondTableColumn'];
                }
            }
            if (count($conditions) > 0) {
                $query = $query . " WHERE ";
                foreach ($conditions as $key => $value) {
                    $values[] = $key . " = " . "'" . $value . "'";
                }
                $query = $query . " " . implode(" AND ", $values);
            }
            $query .= " $otherConditions";

            $query = $this::$conn->prepare($query);
            if ($query->execute()) {
                if ($query->rowCount()) {
                    return true;
                }
            }
            return false;
        } catch (PDOException $e) {
            $this->exceptionDataResult();
        }
        return false;
    }

}