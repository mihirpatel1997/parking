<?php

class Config
{

    protected static $responseCode = '';
    protected static $responseMessage = '';
    protected static $result = '';
    protected static $conn = '';
    protected $inDetails = false;
    protected $data = array();
    protected $insertUpdateFlag = true;

    use CommonFunction;

    public function __construct()
    {
        //Local
        $servername = 'localhost';
        $username = 'root';
        $password = '';
        $dbName = 'parking';

        //Live
        /*$servername = 'localhost';
        $username = 'root';
        $password = '';
        $dbName = 'furniture_hub';*/

        try {

            $this::$conn = new PDO('mysql:host=' . $servername . ';dbname=' . $dbName, $username, $password);
            $this::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    public function successResult($message = 'Success')
    {
        $this::$responseCode = '1';
        $this::$responseMessage = $message;
    }

    public function errorResult($message = 'Error')
    {
        $this::$responseCode = '0';
        $this::$responseMessage = $message;
        $this::$result = '';
    }

    public function noDataResult($message = 'No data')
    {
        $this::$responseCode = '0';
        $this::$responseMessage = $message;
        $this::$result = '';
    }

    public function exceptionDataResult($string = 'Exception error')
    {
        $this::$responseCode = '0';
        $this::$responseMessage = $string;
        $this::$result = '';
    }

    public function apiResponseData()
    {
        $responseArray = array(
            'code' => $this::$responseCode,
            'message' => $this::$responseMessage,
            'result' => $this::$result,
        );
        return json_encode($responseArray);
    }

}