<?php

class Vehicle extends Config
{
    public function addVehicle()
    {
        try {
            $vehicleNo = $this->handleSpecialCharacters($_POST['vehicleNo']);
            $vehicleType = $this->handleSpecialCharacters($_POST['vehicleType']);
            $userId = $this->handleSpecialCharacters($_POST['userId']);

            if ($this->checkDataIsExistInTable('vehicle_master', ['vehicle_no'], array('vehicle_no' => $vehicleNo),array(),' AND status NOT IN(2)') === false) {
                $query = $this::$conn->prepare("INSERT INTO vehicle_master (vehicle_no,vehicle_type,user_id) VALUES ('$vehicleNo','$vehicleType','$userId')");

                if ($query->execute()) {
                    if ($query->rowCount() > 0) {
                        $this->successResult("Vehicle Added");
                    } else {
                        $this->noDataResult("Something went wrong");
                    }
                } else {
                    $this->errorResult();
                }
            } else {
                $this->errorResult("Vehicle number is already added...");
            }
        } catch (PDOException $e) {
            $this->exceptionDataResult();
        }
    }

    public function deleteVehicle()
    {
        try {

            $vehicleId = $this->handleSpecialCharacters($_POST['vehicleId']);

            $query = $this::$conn->prepare("UPDATE vehicle_master SET status='2' WHERE id='$vehicleId'");
            if ($query->execute()) {
                if ($query->rowCount() > 0) {
                    $this->successResult("Vehicle Deleted");
                } else {
                    $this->noDataResult("Something went wrong");
                }
            } else {
                $this->errorResult();
            }
        } catch (PDOException $e) {
            $this->exceptionDataResult();
        }
    }

    public function getVehicleDetails()
    {
        try {
            $userId = $this->handleSpecialCharacters($_POST['userId']);

            $appendQuery = "";
            if ($this->isNotNullOrEmptyOrZero($userId)) {
                $appendQuery = " AND user_id='$userId'";
            }

            $query = $this::$conn->prepare("SELECT vehicle_master.*,user_master.name AS userName FROM vehicle_master 
            LEFT JOIN user_master ON user_master.id = vehicle_master.user_id  WHERE status='1' $appendQuery");

            if ($query->execute()) {
                if ($query->rowCount() > 0) {
                    $this->successResult();
                    foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $row) {
                        $this->data[] = array(
                            'vehicleId' => strval($row['id']),
                            'vehicleNo' => $this->convertNullToEmptyString($row['vehicle_no']),
                            'vehicleType' => $this->convertNullToEmptyString($row['vehicle_type']),
                            'userId' => strval($row['user_id']),
                            'userName' => $this->convertNullToEmptyString($row['userName'])
                        );
                    }

                    $this::$result = array('vehicles' => $this->data);
                } else {
                    $this->noDataResult("No any vehicles");
                }
            } else {
                $this->errorResult();
            }
        } catch (PDOException $e) {
            $this->exceptionDataResult();
        }
    }

    public function addLocation()
    {
        try {
            $locationName = $this->handleSpecialCharacters($_POST['locationName']);
            $latitude = $this->handleSpecialCharacters($_POST['lattitude']);
            $longitude = $this->handleSpecialCharacters($_POST['longitude']);
            $areaName = $this->handleSpecialCharacters($_POST['areaName']);
            $numberOfSlot = $this->handleSpecialCharacters($_POST['numberOfSlot']);
            $slotAmount = $this->handleSpecialCharacters($_POST['slotAmount']);

            $query = $this::$conn->prepare("INSERT INTO `location_master`(`location_name`, `lattitude`, `longitude`, `area_name`, 
            `number_of_slot`, `slot_amount`) VALUES ('$locationName','$latitude','$longitude','$areaName','$numberOfSlot','$slotAmount')");

            if ($query->execute()) {
                if ($query->rowCount() > 0) {
                    $this->successResult("Location Added");
                } else {
                    $this->noDataResult("Something went wrong");
                }
            } else {
                $this->errorResult();
            }
        } catch (PDOException $e) {
            $this->exceptionDataResult();
        }
    }

    public function getLocationDetails()
    {
        try {
            $query = $this::$conn->prepare("SELECT * FROM location_master");
            if ($query->execute()) {
                if ($query->rowCount() > 0) {
                    $this->successResult();
                    foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $row) {
                        $this->data[] = array(
                            'locationId' => strval($row['id']),
                            'locationName' => $this->convertNullToEmptyString($row['location_name']),
                            'latitude' => $this->convertNullToEmptyString($row['lattitude']),
                            'longitude' => $this->convertNullToEmptyString($row['longitude']),
                            'areaName' => $this->convertNullToEmptyString($row['area_name']),
                            'numberOfSlot' => strval($row['number_of_slot']),
                            'slotAmount' => strval($row['slot_amount']),
                            'totalSlotAmount' => strval($row['number_of_slot'] * $row['slot_amount'])
                        );
                    }

                    $this::$result = array('location' => $this->data);
                } else {
                    $this->noDataResult("No any locations");
                }
            } else {
                $this->errorResult();
            }
        } catch (PDOException $e) {
            $this->exceptionDataResult();
        }
    }

    public function booking()
    {
        try {
            $userId = $this->handleSpecialCharacters($_POST['userId']);
            $locationId = $this->handleSpecialCharacters($_POST['locationId']);
            $bookingDate = date('Y-m-d', strtotime($_POST['bookingDate']));
            $bookingTime = date('H:i:s', strtotime($_POST['bookingTime']));
            $bookingEndTime = date('H:i:s', strtotime($_POST['bookingEndTime']));
            $slotId = json_decode($_POST['slotId'], true);
            $vehicleId = json_decode($_POST['vehicleId'], true);

            $bookingDateAndTime = $bookingDate . ' ' . $bookingTime;
            $bookingEndDateAndTime = $bookingDate . ' ' . $bookingEndTime;
            $uniqId = uniqid();
            $bookingIds = array();

            foreach ($slotId AS $key => $slot) {
                $query = $this::$conn->prepare("INSERT INTO `booking_master`(`user_id`, `vehicle_id`, `location_id`, `slot_id`,`booking_date_time`, 
            `booking_end_date_time`, `unique_id`, `bassic_amount`, `net_amount`) VALUES
            ('$userId','$vehicleId[$key]','$locationId','$slot','$bookingDateAndTime','$bookingEndDateAndTime','$uniqId','20','20')");
                $query->execute();
                $bookingIds[] = $this::$conn->lastInsertId();

            }
            $this->successResult("Booking Success");
            $this::$result = array("bookingIds" => $bookingIds);

        } catch (PDOException $e) {
            $this->exceptionDataResult();
        }
    }

    public function getBookingDetails()
    {
        try {

            $bookingIds = json_decode($_POST["bookingIds"], true);
            $userId = $this->handleSpecialCharacters($_POST['userId']);
            $type = $this->handleSpecialCharacters($_POST['type']);

            $appendQuery = "";
            if ($this->isNotNullOrEmptyOrZero($userId)) {
                $appendQuery = " WHERE booking_master.user_id='$userId'";
            }
            if ($this->isNotNullOrEmptyOrZero($bookingIds)) {
                $appendQuery = " WHERE booking_master.id IN ('" . implode("','", $bookingIds) . "')";
            }

            $currentDate = date("Y-m-d H:i:s");
            if ($this->equals($type, 'previous')) {
                $appendQuery .= " AND booking_date_time < '$currentDate' ORDER BY booking_date_time DESC";
            } else if ($this->equals($type, 'upcoming')) {
                $appendQuery .= " AND booking_date_time >= '$currentDate' ORDER BY booking_date_time DESC";
            }

            $query = $this::$conn->prepare("SELECT booking_master.*,user_master.name AS userName,vehicle_master.vehicle_no AS vehicleNo,
            vehicle_master.vehicle_type AS vehicleType, location_master.location_name AS locationName,location_master.area_name AS areaName,
            location_master.lattitude,location_master.longitude,
            slot_master.slot_no AS slotNo 
            FROM booking_master 
            LEFT JOIN user_master ON user_master.id = booking_master.user_id
            LEFT JOIN vehicle_master ON vehicle_master.id = booking_master.vehicle_id
            LEFT JOIN location_master ON location_master.id = booking_master.location_id
            LEFT JOIN slot_master on slot_master.id = booking_master.slot_id
            $appendQuery");

            if ($query->execute()) {
                if ($query->rowCount() > 0) {
                    $this->successResult();
                    foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $row) {
                        $this->data[] = array(
                            'bookingId' => strval($row['id']),
                            'userId' => strval($row['user_id']),
                            'userName' => $this->convertNullToEmptyString($row['userName']),
                            'vehicleId' => strval($row['vehicle_id']),
                            'vehicleNo' => $this->convertNullToEmptyString($row['vehicleNo']),
                            'vehicleType' => $this->convertNullToEmptyString($row['vehicleType']),
                            'locationId' => strval($row['location_id']),
                            'locationName' => $this->convertNullToEmptyString($row['locationName']),
                            'areaName' => $this->convertNullToEmptyString($row['areaName']),
                            'bookingDate' => $this->convertNullToEmptyString(date('d-m-Y', strtotime($row['booking_date_time']))),
                            'bookingTime' => $this->convertNullToEmptyString(date('H:i:s', strtotime($row['booking_date_time']))),
                            'bookingEndDate' => $this->convertNullToEmptyString(date('d-m-Y', strtotime($row['booking_end_date_time']))),
                            'bookingEndTime' => $this->convertNullToEmptyString(date('H:i:s', strtotime($row['booking_end_date_time']))),
                            'uniqueId' => strval($row['unique_id']),
                            'basicAmount' => $this->convertNullToEmptyString($row['bassic_amount']),
                            'netAmount' => $this->convertNullToEmptyString($row['net_amount']),
                            'slotId' => strval($row['slot_id']),
                            'slotNo' => strval($row['slotNo']),
                            'areaLatitude' => strval($row['lattitude']),
                            'areaLongitude' => strval($row['longitude']),
                        );
                    }

                    $this::$result = array('booking' => $this->data);
                } else {
                    $this->noDataResult("No any bookings");
                }
            } else {
                $this->errorResult();
            }
        } catch (PDOException $e) {
            $this->exceptionDataResult();
        }
    }

    public function checkSlotDetails()
    {
        try {
            $locationId = $this->handleSpecialCharacters($_POST['locationId']);
            $bookingDateTime = date('Y-m-d H:i:s', strtotime($_POST['bookingStartDateTime']));
            $bookingEndDateTime = date('Y-m-d H:i:s', strtotime($_POST['bookingEndDateTime']));

            $slotsArray = array();
            $query = $this::$conn->prepare("SELECT * FROM slot_master WHERE location_id='$locationId'");
            if ($query->execute()) {
                if ($query->rowCount() > 0) {

                    foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $row) {
                        $slotId = $row['id'];
                        $isBooked = false;
                        $checkBooking = $this::$conn->prepare("SELECT * FROM booking_master WHERE location_id='$locationId' AND slot_id='$slotId'
                        AND booking_date_time='$bookingDateTime' AND booking_end_date_time='$bookingEndDateTime'");

                        //var_dump($checkBooking);
                        if ($checkBooking->execute()) {
                            if ($checkBooking->rowCount() > 0) {
                                foreach ($checkBooking->fetchAll(PDO::FETCH_ASSOC) as $rows) {
                                    $isBooked = true;

                                }
                            }
                        }

                        $slotsArray[] = array(
                            'slotId' => strval($row['id']),
                            'slotNo' => strval($row['slot_no']),
                            'price' => strval($row['price']),
                            'isBooked' => $isBooked,
                            'isSelected' => false
                        );
                    }

                    $this->successResult();
                    $this::$result = array('slots' => $slotsArray);
                } else {
                    $this->noDataResult();
                }
            } else {
                $this->errorResult();
            }
        } catch (PDOException $e) {
            $this->exceptionDataResult();
        }
    }

    public function addDummyData()
    {
        try {

            $truncate = $this::$conn->prepare("TRUNCATE TABLE slot_master");
            $truncate->execute();

            $dummy_data = array();
            $location = array();

            $getLocation = $this::$conn->prepare("SELECT id,slot_amount FROM  location_master");
            if ($getLocation->execute()) {
                if ($getLocation->rowCount() > 0) {
                    foreach ($getLocation->fetchAll(PDO::FETCH_ASSOC) as $rows) {
                        $location[] = array(
                            'locationId' => $rows['id'],
                            'slotAmount' => $rows['slot_amount']
                        );
                    }
                }
            }

            foreach ($location AS $val) {
                for ($i = 1; $i <= 20; $i++) {
                    $dummy_data[] = array(
                        "location_id" => $val['locationId'],
                        "slot_no" => "A{$i}",
                        "price" => $val['slotAmount']
                    );
                }
            }

            $flag = false;
            foreach ($dummy_data AS $value) {
                $lid = $value['location_id'];
                $slot_no = $value['slot_no'];
                $price = $value['price'];
                $query = $this::$conn->prepare("INSERT INTO slot_master (location_id,slot_no,price) VALUES ('$lid','$slot_no','$price')");
                if ($query->execute()) {
                    $flag = true;
                }

            }
            if ($flag) {
                echo 'Success';
                exit;
            } else {
                echo 'Fail';
                exit;
            }


        } catch (PDOException $e) {
            $this->exceptionDataResult();
        }
    }
}
