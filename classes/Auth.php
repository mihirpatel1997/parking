<?php

class Auth extends Config
{
    public function login()
    {
        try {
            $mobileNumber = $this->handleSpecialCharacters($_POST['mobileNumber']);
            $password = $this->handleSpecialCharacters($_POST['password']);

            $query = $this::$conn->prepare("SELECT * FROM user_master WHERE mobile IN ('$mobileNumber') AND password IN ('$password')");
            
            if ($query->execute()) {
                if ($query->rowCount() > 0) {
                    $this->successResult("User Logged");
                    $this->data = $query->fetch(PDO::FETCH_ASSOC);
                    
                    $this::$result = array('user' => $this->data);
                } else {
                    $this->noDataResult("User Name or Password is Incorrect!!!");
                }
            } else {
                $this->errorResult();
            }
        } catch (PDOException $e) {
            $this->exceptionDataResult();
        }
    }

    public function register()
    {
        try {
            $userName = $this->handleSpecialCharacters($_POST['userName']);
            $mobileNumber = $this->handleSpecialCharacters($_POST['mobileNumber']);
            $emailAddress = $this->handleSpecialCharacters($_POST['emailAddress']);
            $password = $this->handleSpecialCharacters($_POST['password']);

            if ($this->checkDataIsExistInTable('user_master', ['mobile'], array('name' => $userName)) === false) {
                $query = $this::$conn->prepare("INSERT INTO user_master (name,mobile,email,password) 
                VALUES ('$userName','$mobileNumber','$emailAddress','$password')");

                if ($query->execute()) {
                    if ($query->rowCount() > 0) {
                        $this->successResult("User Register");
                    } else {
                        $this->noDataResult("Failed to register");
                    }
                } else {
                    $this->errorResult();
                }
            } else {
                $this->errorResult("Mobile number is already registered...");
            }
        } catch (PDOException $e) {
            $this->exceptionDataResult();
        }
    }
}
